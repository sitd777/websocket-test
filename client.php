<?php

require(__DIR__ . '/vendor/autoload.php');

use client\Client;

$client = new Client($argv);
$client->processCommand();

