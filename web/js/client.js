// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>> WSClient
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

function WSClient() {
    this.wsCtor = window['MozWebSocket'] ? MozWebSocket : WebSocket;
    if (!this.wsCtor) {
        alert("Your browser doesn't seem to support WebSocket.");
    }

    this.messageHandlers = {
        list: this.commandList.bind(this),
        msg:  this.commandMessage.bind(this),
    };

    this.initUI();
};

WSClient.prototype.user = 0;
WSClient.prototype.task = 0;
WSClient.prototype.url = 'ws://localhost:8888';
WSClient.prototype.wsCtor = null;
WSClient.prototype.socket = null;
WSClient.prototype.messageHandlers = [];

WSClient.prototype.initUI = function() {
    var self = this;
    $('#user').change(function() { self.user = $(this).val(); }).change();
    $('#task').change(function() { self.task = $(this).val(); }).change();

    $('#connectButton').click(function() { self.connect(); });
    $('#disconnectButton').click(function() { self.disconnect(); });
    $('#sendButton').click(function() { self.sendMsg(); });
};

WSClient.prototype.connect = function() {
    var ws = this.wsCtor;
    this.socket = new ws(this.url + '?user=' + this.user + '&task=' + this.task);

    this.socket.onmessage = this.handleWebsocketMessage.bind(this);
    this.socket.onclose = this.handleWebsocketClose.bind(this);
    this.socket.onopen = this.handleWebsocketOpen.bind(this);

    $('#user, #task').attr('disabled', true);
    $('.input').attr('disabled', false);
    $('#connectButton').hide();
    $('#disconnectButton').show();
};

WSClient.prototype.handleWebsocketMessage = function(message) {
    try {
        var command = JSON.parse(message.data);
    }
    catch(e) { /* do nothing */  }

    if (command) {
        this.dispatchCommand(command);
        console.log("WebSocket Command: " + message.data);
    }
};

WSClient.prototype.dispatchCommand = function(command) {
    // Do we have a handler function for this command?
    var handler = this.messageHandlers[command.cmd];

    if (typeof(handler) === 'function') {
        // If so, call it and pass the parameter data
        handler.call(this, command.data);
    }
};

WSClient.prototype.handleWebsocketOpen = function() {
};

WSClient.prototype.sendMsg = function() {
    if(this.socket.readyState != 1) {
        return;
    }

    var msg = $('#msgText').val().trim();
    if(msg.length == 0) {
        alert('Сообщение не может быть пустым!');
        return;
    }
    var user = $('#msgUser').val().trim();
    var task = $('#msgTask').val().trim();
    var command = JSON.stringify({ cmd: "msg", data: { msg: msg, user: user, task: task }});

    this.socket.send(command);
    console.log("Command sended: " + command);
};

WSClient.prototype.handleWebsocketClose = function() {
    $('#user, #task').attr('disabled', false);
    $('.input').attr('disabled', true);
    $('#disconnectButton').hide();
    $('#connectButton').show();
    $('#participants li').remove();
    $('#messages li').remove();
    alert("Connection closed.");
};

WSClient.prototype.disconnect = function() {
    this.socket.close();
};

WSClient.prototype.commandList = function(data) {
    $('#participants li').remove();
    for(var idx in data) {
        var info = data[idx];
        var button = (info.user == this.user && info.task == this.task) ?
            '<span class="label label-default pull-right">Я</span>' :
            '<button class="btn btn-xs btn-primary pull-right" title="Написать">&raquo;</button>';
        $('#participants').append('<li class="list-group-item justify-content-between" data-key="' + idx +
            '" data-task="' + info.task + '" data-user="' + info.user + '">User: ' + info.user +
            '; Task: ' + info.task + button + '</li>');
    }
    $('#participants li button').click(this.setRecipient);
};

WSClient.prototype.commandMessage = function(data) {
    var my = (data.user == this.user && data.task == this.task);
    var owner = '';
    if (data.user == 0 || data.task == 0) owner = '<span class="label label-danger">Система</span>';
    else if (my) owner = '<span class="label label-primary">Я</span>';
    else owner = '<span class="label label-warning">User: ' + data.user + '; Task: ' + data.task + ';</span>';

    var html = '<div class="panel ' + (my ? 'panel-primary my' : 'panel-danger') + '" data-task="' + data.task +
        '" data-user="' + data.user + '"><div class="panel-body"><span class="label label-default">' + data.time + '</span> ' +
        owner + ' ' + data.msg + '</div></div>';
    $('#messages').prepend(html);
};

WSClient.prototype.setRecipient = function() {
    var li = $(this).closest('li');
    $('#msgUser').val(li.data('user'));
    $('#msgTask').val(li.data('task'));
}
