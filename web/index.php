<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP test project</title>
    <link rel="stylesheet" href="bootstrap-3.2.0-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/site.css">
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/client.js"></script>
    <script src="js/site.js"></script>
</head>
<body>
<div class="wrap">
    <nav id="w0" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">PHP test project</a>
            </div>
        </div>
    </nav>
    <div class="container main">
        <section class="messages">
        </section>
        <section class="content">
            <div class="row">
                <div class="col-sm-3">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Соединение</h3>
                        </div>
                        <div class="panel-body">
                            <div id="connectGroup">
                                <div class="form-group">
                                    <label for="user">User ID</label>
                                    <input type="text" class="form-control" id="user" placeholder="User" value="<?= rand( 1, 99) ?>">
                                </div>
                                <div class="form-group">
                                    <label for="user">Task ID</label>
                                    <input type="text" class="form-control" id="task" placeholder="Task" value="<?= rand( 1, 99) ?>">
                                </div>
                                <button class="btn btn-primary" id="connectButton">Соединить</button>
                                <button class="btn btn-danger" style="display: none;" id="disconnectButton">Разъединить</button>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Участники</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group" id="participants">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="panel panel-primary panel-messages">
                        <div class="panel-heading">
                            <h3 class="panel-title">Сообщения</h3>
                        </div>
                        <div class="panel-body">
                            <div id="messages">
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="user" class="control-label">User:</label>
                                        <input type="text" class="form-control input" disabled="true" id="msgUser" placeholder="All" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="user" class="control-label">Task:</label>
                                        <input type="text" class="form-control input" disabled="true" id="msgTask" placeholder="All" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required">
                                        <label for="user" class="control-label">Cообщение:</label>
                                        <textarea id="msgText" class="form-control input" disabled="true"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary input" disabled="true" id="sendButton">Отправить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</body>
</html>