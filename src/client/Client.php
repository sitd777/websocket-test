<?php

namespace client;

use WebSocket\Client as WSClient;

final class Client {
    /**
     * Holds console parameters
     * @var array
     */
    private $args = [];

    /**
     * Holds available commands
     * @var array
     */
    private $commands = [
        'help' => ['desc' => 'client.php help -> Выводит данную информацию'],
        'get-all-users' => ['desc' => 'client.php get-all-users -> вывести на экран ID всех зарегистрированных на WebSocket сервере пользователей.'],
        'get-all-user-task' => ['desc' => 'client.php get-all-user-task=userId -> вывести на эран ID всех зарегистрированных на WebSocket сервере задач одного пользователя.'],
        'send-message' => [
            'desc' => [
                'client.php send-message=all message="Текст сообщения" -> отправить сообщение всем зарегистрированным на WebSocket сервере пользователям, во все задачи.',
                'client.php send-message=userId message="Текст сообщения" -> отправить сообщение одному зарегистрированному на WebSocket сервере пользователю, во все задачи.',
                'client.php send-message=userId task=taskId message="Текст сообщения" -> отправить сообщение одному зарегистрированному на WebSocket сервере пользователю, в одну задачу.',
            ],
            'params' =>['task', 'message']
        ],
    ];

    /**
     * Client constructor.
     * @param array $consoleVars
     */
    public function __construct($consoleVars)
    {
        // Unset file name
        unset($consoleVars[0]);

        // Prepare data
        $this->args = [];
        foreach ($consoleVars as $value) {
            $value = explode('=', $value, 2);
            $this->args[$value[0]] = isset($value[1]) ? $value[1] : 0;
        }
    }

    /**
     * Processes command line action
     */
    public function processCommand()
    {
        // Get command
        $cmd = 'help';
        foreach($this->args as $var => $value)
        {
            $var = strtolower($var);
            if(isset($this->commands[$var])) {
                $cmd = $var;
                break;
            }
        }

        // Prepare action name
        $actionName = explode('-', $cmd);
        foreach($actionName as $idx => $part)
        {
            $actionName[$idx] = ucfirst(strtolower($part));
        }
        $actionName = 'action' . implode('', $actionName);

        // Prepare parameters
        $params = [];
        $params[] = $this->args[$cmd];
        unset($this->args[$cmd]);
        if(isset($this->commands[$cmd]['params']))
        {
            foreach($this->commands[$cmd]['params'] as $paramName)
            {
                $params[] =  isset($this->args[$paramName]) ? $this->args[$paramName] : 0;
            }
        }

        // Execute action
        if (method_exists($this, $actionName)) {
            call_user_func_array([$this, $actionName], $params);
        } else {
            $this->actionHelp();
        }
    }

    /**
     * Shows help information
     */
    private function actionHelp()
    {
        foreach($this->commands as $command => $commandData)
        {
            $desc = @$commandData['desc'];
            if(!is_array($desc)) $desc = [$desc];
            foreach($desc as $commandDescription)
            {
                echo $commandDescription . "\n\n";
            }
        }
    }

    /**
     * Shows all available users
     */
    private function actionGetAllUsers()
    {
        $client = new WSClient("ws://127.0.0.1:8888/");
        $client->send(json_encode(['cmd' => 'users']));
        $msg = (array)json_decode($client->receive());
        $data = (array)@$msg['data'];

        echo "Доступные пользователи:\n";
        if (count($data))
        {
            foreach ($data as $user)
            {
                echo $user . "\n";
            }
        } else {
            echo "Здесь никого...\n";
        }
    }

    /**
     * Shows all available users
     */
    private function actionGetAllUserTask($id)
    {
        $id = (int)$id;
        if (!$id) {
            echo "Необходимо указать userId !\n";
            die;
        }

        $client = new WSClient("ws://127.0.0.1:8888/");
        $client->send(json_encode(['cmd' => 'usertasks', 'data' => ['id' => $id]]));
        $msg = (array)json_decode($client->receive());
        $data = (array)@$msg['data'];

        echo "Доступные задачи пользователя " . $id . ":\n";
        if (count($data))
        {
            foreach ($data as $user)
            {
                echo $user . "\n";
            }
        } else {
            echo "Ничего нет...\n";
        }
    }

    /**
     * Sends message to users/tasks
     */
    private function actionSendMessage($userId, $taskId, $message)
    {
        $message = trim(strip_tags($message));
        if (!$message) {
            echo "Необходимо написать сообщение !\n";
            die;
        }
        $taskId = (int)$taskId;
        $userId = (int)$userId;
        $client = new WSClient("ws://127.0.0.1:8888/");
        $client->send(json_encode(['cmd' => 'msg', 'data' => ['user' => $userId, 'task' => $taskId, 'msg' => $message]]));
        echo "Сообщение отправлено !\n";
    }
}
