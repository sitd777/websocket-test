<?php

namespace server;

use Ratchet\ConnectionInterface;

final class Connection
{
    /**
     * WebSocket connection
     * @var ConnectionInterface
     */
    public $connection = null;

    /**
     * WebSocket connection ID
     * @var integer
     */
    public $rid = 0;

    /**
     * Connection user
     * @var integer
     */
    public $user = 0;

    /**
     * Connection task
     * @var integer
     */
    public $task = 0;

    /**
     * Connection constructor.
     */
    public function __construct(ConnectionInterface $connection, $rid, $user, $task)
    {
        $this->connection = $connection;
        $this->rid = $rid;
        $this->user = $user;
        $this->task = $task;
    }
}