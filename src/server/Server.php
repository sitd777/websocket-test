<?php
namespace server;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class Server implements MessageComponentInterface
{
    /**
     * Available connections
     * @var ConnectionInterface[]
     */
    protected $clients = [];

    /**
     * Connections sorted by user / task
     * @var array
     */
    protected $clientsByData = [];

    /**
     * @var array list of available requests
     */
    protected $requests = [
        'list',
        'msg',
        'users',
        'usertasks',
    ];

    /**
     * Holds server start time
     * @var int
     */
    protected $_startTime = 0;

    /**
     * Server constructor.
     */
    public function __construct()
    {
        $this->_startTime = time();
        $this->log('WebSocket server started.');
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $rid = $this->getResourceId($conn);

        $query = $conn->WebSocket->request->getQuery();
        $user = (int)$query->get('user');
        $task = (int)$query->get('task');

        $connection = new Connection($conn, $rid, $user, $task);
        $this->clients[$rid] = $connection;
        if($user && $task) {
            $this->clientsByData[$user][$task] = $connection;
        }

        $this->log('Connection is established: RID ' . $rid . '; User: ' . $user . '; Task: ' . $task);

        // Send fixed list of recepients to others
        if($user && $task) {
            $this->sendToAll('list', $this->getRecipientsList());
        }
    }

    /**
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        // Prepare data
        $data = (array)json_decode($msg);
        $cmd = @$data['cmd'];
        unset($data['cmd']);

        $rid  = $this->getResourceId($from);
        $this->log('Message received from ' . $rid . ': ' . $msg);

        if($cmd == '') return;

        /* @var ServerConnection $sConn */
        $sConn = $this->clients[$rid];
        if(!$sConn) return;

        // Execute message processing
        if (in_array($cmd, $this->requests)) {
            call_user_func_array([$this, 'request' . ucfirst(strtolower($cmd))], [$sConn, (array)$data['data']]);
        } else {
            $this->log('Unknown command from ' . $rid . ': ' . $cmd);
        }
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $rid  = $this->getResourceId($conn);

        /* @var ServerConnection $sConn */
        $sConn = $this->clients[$rid];
        if(!$sConn) return;

        $connection = $this->clients[$rid];

        unset($this->clients[$rid]);
        unset($this->clientsByData[$connection->user][$connection->task]);

        $this->log('Closed connection: RID ' . $rid . '; User: ' . $connection->user . '; Task: ' . $connection->task);

        // Send fixed list of recepients to others
        if($connection->user && $connection->task) {
            $this->sendToAll('list', $this->getRecipientsList());
        }
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $error = $e->getMessage();
        $conn->send(json_encode(['cmd' => 'error', 'data' => [
            'message' => $error,
        ]]));
        $this->log('Error: ' . $error . '; File: ' . $e->getFile() . '; Line: ' . $e->getLine());
        $conn->close();
    }

    /**
     * Prints message to log
     *
     * @param string $msg
     */
    protected function log($msg)
    {
        echo date('d.m.Y h:i:s') . ": ".$msg . "\r\n";
    }

    /**
     * Get connection resource id
     *
     * @access private
     * @param ConnectionInterface $conn
     * @return string
     */
    private function getResourceId(ConnectionInterface $conn)
    {
        return $conn->resourceId;
    }

    /**
     * Send list of recipients
     * @param Connection $conn
     * @param array $data
     */
    private function requestList(Connection $conn, $data)
    {
        $this->sendToOne($conn, 'list', $this->getRecipientsList());
    }

    /**
     * Send list of users
     * @param Connection $conn
     * @param array $data
     */
    private function requestUsers(Connection $conn, $data)
    {
        $this->sendToOne($conn, 'users', array_keys($this->clientsByData));
    }

    /**
     * Send list of tasks for user
     * @param Connection $conn
     * @param array $data
     */
    private function requestUsertasks(Connection $conn, $data)
    {
        $userId = isset($data['id']) ? (int)$data['id'] : 0;
        $tasks = isset($this->clientsByData[$userId]) ? array_keys($this->clientsByData[$userId]) : array();
        $this->sendToOne($conn, 'users', $tasks);
    }

    /**
     * Sends message
     * @param Connection $conn
     * @param array $data
     */
    private function requestMsg(Connection $conn, $data)
    {
        $sended = [];
        $msgUser = (int)@$data['user'];
        $msgTask = (int)@$data['task'];

        // Clear message
        $data['msg'] = trim(strip_tags(@$data['msg']));

        // Set sender info & time
        $data['user'] = $conn->user;
        $data['task'] = $conn->task;
        $data['time']  = date('d.m.Y H:i:s');
        if(empty($data['msg'])) {
            $this->log('Empty message from RID: ' . $conn->rid);
            return;
        }

        // Send to recipients
        foreach ($this->clientsByData as $user => $userTasks)
        {
            if($msgUser > 0 && $msgUser != $user) continue;
            foreach ($userTasks as $task => $msgConn)
            {
                if($msgTask > 0 && $msgTask != $task) continue;

                $this->sendToOne($msgConn, 'msg', $data);
                $sended[$user][$task] = 1;
            }
        }

        // Send to sended if not sended yet
        if(!isset($sended[$conn->user][$conn->task])) {
            $this->sendToOne($conn, 'msg', $data);
        }
    }

    /**
     * Prepares array of recipients
     * @return array
     */
    private function getRecipientsList()
    {
        $data = [];
        foreach ($this->clientsByData as $user => $userTasks)
        {
            foreach ($userTasks as $task => $conn)
            {
                $id = $user . '.' . $task;
                $data[$id] = [
                    'user' => $user,
                    'task' => $task,
                ];
            }
        }
        return $data;
    }

    /**
     * Send data to one recipient
     * @param Connection $conn
     * @param string $cmd
     * @param array $data
     */
    private function sendToOne(Connection $conn, $cmd, $data)
    {
        $json = json_encode(['cmd' => $cmd, 'data' => $data]);
        $conn->connection->send($json);
    }

    /**
     * Send data to all recipients
     * @param string $cmd
     * @param array $data
     */
    private function sendToAll($cmd, $data)
    {
        $json = json_encode(['cmd' => $cmd, 'data' => $data]);
        foreach ($this->clients as $conn)
        {
            $conn->connection->send($json);
        }
    }
}