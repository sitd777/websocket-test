<?php

require(__DIR__ . '/vendor/autoload.php');

use \Ratchet\Server\IoServer;
use \Ratchet\Http\HttpServer;
use \Ratchet\WebSocket\WsServer;
use \server\Server;

ini_set("memory_limit", -1);
$pid = getmypid();
echo "Server PID: " . $pid . "\r\n";

$port = 8888;
echo "Server port: " . $port . "\r\n";

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Server()
        )
    ),
    $port
);
$server->run();